
class Car:
    def __init__(self,type,color,number_of_doors):
        self.type=type
        self.color=color
        self.number_of_doors=number_of_doors

    def setNumberOfDoors(self,number):
        self.number_of_doors=number
    def setColor(self,col):
        self.color=col
    def setType(self,t):
        self.type=t
    @property
    def getColor(self):
        print(f'getting color', self.color)
        return self.color
    @property
    def getType(self):
        print(f'getting type',self.type)
        return self.type
    @property
    def getNumberOfDoors(self):
        print(f'getting no.doors',self.number_of_doors)
        return self.number_of_doors


class BMW(Car):
    def __init__(self,mark,year,tools):
        self.mark=mark
        self.year=year
        self.tools=tools

    def setMark(self,mark):
        self.mark=mark
    def setYear(self,year):
        self.year=year
    def setTools(self,tool):
        self.tools=tool

    @property
    def getYear(self):
        print(f'getting year', self.year)
        return self.year
    @property
    def getMark(self):
        print(f'getting mark', self.mark)
        return self.mark
    @property
    def getTools(self):
        print(f'getting tools', self.tools)
        return self.tools


# car=Car('SUV','red',4)
# car.getType
# car.getColor
# print('-------------------------------------------------------------\n')
# car2=BMW('M5','2018',True)
# car2.setType('Sedan')
# car2.setColor('black metallic')
#
# car2.getType
# car2.getColor
# car2.getMark
# car2.getYear
# car2.getTools

# # Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#     c=Car()


