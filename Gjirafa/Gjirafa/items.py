# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class GjirafaItem(scrapy.Item):
    # define the fields for your item here like:
    name=scrapy.Field()
    prev_price=scrapy.Field()
    curr_price=scrapy.Field()
    discount_precentage=scrapy.Field()
    links=scrapy.Field()

    # Housekeeping fields
    url = scrapy.Field()
    project = scrapy.Field()
    spider = scrapy.Field()
    server = scrapy.Field()
    date = scrapy.Field()
