import scrapy


class DiscountspidergjirafaSpider(scrapy.Spider):
    name = 'DiscountSpiderGjirafa'
    allowed_domains = ['gjirafa50.com']
    start_urls = ['http://gjirafa50.com/']

    def parse(self, response):
        for product in response.css('div.ty-column5'):
            if product.css('span.ty-discount-label__value::text').get() != None:
                yield {
                    'name': product.css('a.product-title::text').get(),
                    'prev_price': product.css('span.ty-list-price::text').get(),
                    'curr_price': product.css('span.ty-price-num::text').get(),
                    'discount_pecentage': product.css('span.ty-discount-label__value::text').get(),
                }
