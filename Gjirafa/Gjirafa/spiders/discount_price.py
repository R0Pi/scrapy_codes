import datetime
import socket

import scrapy
from Gjirafa.items import GjirafaItem
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, Join

class DiscountGamingLaptopSpider(scrapy.Spider):
    name='DiscountGamingLaptop'
    allowed_domains = ['https://gjirafa50.com']

    start_urls=[
        f'https://gjirafa50.com/kompjutere-laptop/laptop/gaming/page-{i}' for i in range(32)
    ]
    def parse(self,response):

        """ This function parses a property page.
        @url https://gjirafa50.com
        @returns items 1
        @scrapes title price description address image_urls
        @scrapes url project spider server date
        """

        for sel in response.css('div.ty-column4'):
            if sel.css('span.ty-discount-label__value::text').get()!= None:
                if sel.css('div.sold-out').get() is None:
                    item = ItemLoader(item=GjirafaItem(), selector=sel)
                    item.add_css('name','a.product-title::text')
                    item.add_css('prev_price','span.ty-list-price::text',MapCompose(lambda i: i.replace(',', ''), float),re='[,.0-9]+')
                    item.add_css('curr_price','span.ty-price-num::text',MapCompose(lambda i: i.replace(',', ''), float),re='[,.0-9]+')
                    item.add_css('discount_precentage','span.ty-discount-label__value::text')
                    item.add_css('links','a.product-title::attr(href)')

                    #Housekeeping fields
                    item.add_value('url', response.url)
                    item.add_value('project', self.settings.get('BOT_NAME'))
                    item.add_value('spider', self.name)
                    item.add_value('server', socket.gethostname())
                    item.add_value('date', datetime.datetime.now())

                    yield item.load_item()

#
#-----------------------------------------------------------------------------------------------------
                # yield{
                #     'name':product.css('a.product-title::text').get(),
                #     'prev_price':product.css('span.ty-list-price::text').get(),
                #     'curr_price':product.css('span.ty-price-num::text').get(),
                #     'discount_pecentage':product.css('span.ty-discount-label__value::text').get(),
                # }




        # yield from response.follow_all(css='div.ty-pagination__items a',callback=self.parse)