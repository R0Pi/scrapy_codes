import scrapy
import json

from itemloaders import ItemLoader
from fastinvest.items import FastinvestItem

class LoanListSpider(scrapy.Spider):
    name = 'loan_list'
    allowed_domains = ['https://www.fastinvest.com/en']
    start_urls = ['https://www.fastinvest.com/en/loan-list']

    def parse(self, response):
        to_json=json.loads(response.text)
        for data in to_json['data']:
            loan=ItemLoader(item=FastinvestItem(),response=data)

            loan.add_value('loanId',data['alias'])

            loan.add_value('dateOfIssue',data['dateOfIssue'])
            loan.add_value('currencySymbol',data['currencySymbol'])
            loan.add_value('amount',f"{data['amount']}")
            loan.add_value('interesRate',data['interest_rate'])
            loan.add_value('timeRemaining',data['timeRemaining'])
            loan.add_value('availableInvestment',data['availableInvestment'])

            yield loan.load_item()



