# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import sqlite3

class FastinvestPipeline:

    def __init__(self):
        self.con=sqlite3.connect('fastinvest.db')
        self.cur=self.con.cursor()
        self.create_table()

    def create_table(self):
        self.cur.execute("""CREATE TABLE IF NOT EXISTS loans(
        loanId REAL PRIMARY KEY,
        dateOfIssue TEXT,
        currencySymbol TEXT, 
        amount REAL,
        interesRate REAL,
        timeRemaining TEXT,
        availableInvestment REAL
        )""")
    def process_item(self, item, spider):
        currency = lambda x : "EUR" if (x == '€') else x
        item['amount']=f"{item['amount']} {currency(item['currencySymbol'])}"
        
        self.cur.execute("""INSERT OR IGNORE INTO loans VALUES(?,?,?,?,?,?,?)""",
                         (item['loanId'],item['dateOfIssue'],item['currencySymbol'],item['amount'],item['interesRate'],item['timeRemaining'],item['availableInvestment'])
                    )
        self.con.commit()
        return item


