# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst

class FastinvestItem(scrapy.Item):
    loanId=scrapy.Field(output_processor=TakeFirst())
    dateOfIssue=scrapy.Field(output_processor=TakeFirst())
    currencySymbol=scrapy.Field(output_processor=TakeFirst())
    amount=scrapy.Field(output_processor=TakeFirst())
    interesRate=scrapy.Field(output_processor=TakeFirst())
    timeRemaining=scrapy.Field(output_processor=TakeFirst())
    availableInvestment=scrapy.Field(output_processor=TakeFirst())

