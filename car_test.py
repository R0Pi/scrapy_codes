import unittest
from car import Car

class TestCar(unittest.TestCase):
    def setUp(self):
        self.car1 = Car('SUV', 'red', 4)
    def tearDown(self):
        pass
    def test_Car_setMethods(self):

        self.car1.setType('sedan')
        self.car1.setColor('blue')
        self.car1.setNumberOfDoors(5)
        self.assertEqual(self.car1.type, 'sedan')
        self.assertEqual(self.car1.color, 'blue')
        self.assertEqual(self.car1.number_of_doors, 5)
    def test_Car_getMethods(self):
        self.assertEqual(self.car1.getType,'SUV')
        self.assertEqual(self.car1.getColor, 'red')
        self.assertEqual(self.car1.getNumberOfDoors, 4)

if __name__ == '__main__':
        unittest.main()