# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class TelegrafiTechItem(scrapy.Item):
    title=scrapy.Field()
    permalink=scrapy.Field()
    content=scrapy.Field()
    published_at=scrapy.Field()
