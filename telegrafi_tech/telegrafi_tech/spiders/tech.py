import scrapy
import json
from ..items import TelegrafiTechItem
from datetime import datetime
class TechSpider(scrapy.Spider):
    name = 'tech'
    allowed_domains = ["https://telegra.fi/storage/trending/daily/"]
    start_urls = ["https://telegra.fi/storage/trending/daily/teknologji.json",
                  "https://telegra.fi/storage/trending/hourly/teknologji.json",
                  "https://telegra.fi/storage/trending/now/teknologji.json",
                  "https://telegra.fi/storage/trending/two_days/teknologji.json"]

    def parse(self, response):
        all_news=json.loads(response.text)
        for news in all_news:
            if news['category_info']['name']=='Teknologji':
                if "2021"== news['published_at'][0:4]:
                    yield TelegrafiTechItem(
                        title=news['title'],
                        permalink=news['permalink'],
                        content=news['content'],
                        published_at=news['published_at'])


